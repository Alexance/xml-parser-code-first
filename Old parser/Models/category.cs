﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XmlParser.Models
{
    [XmlAttribute("id", "categoryId")]
    [XmlAttribute("parentId", "parentId")]
    [XmlAttributeValue("value")]
    public class category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int categoryId { get; set; }

        [XmlAttributeDontParse]
        public category parent { get; set; }
        [NotMapped]
        public int parentId { get; set; }

        public string value { get; set; }
    }
}
