﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Reflection;
using System.Linq;
using System.Globalization;
using System.Collections.ObjectModel;

namespace XmlParser
{
    /// <summary>
    /// Парсер данных по некоторой модели - Code First
    /// </summary>
    public class XmlParser
    {
        private readonly IXmlDataProvider xmlSource; // Источник XML-информации

        /// <summary>
        /// Инициализация класса некоторым источником данных
        /// </summary>
        /// <param name="xmlSource">Источник данных, реализующий требуемый интерфейс</param>
        public XmlParser(IXmlDataProvider xmlSource)
        {
            this.xmlSource = xmlSource;
        }

        /// <summary>
        /// Парсинг данных из источника XML в коллекцию
        /// </summary>
        /// <typeparam name="T">Прототип парсимого класса</typeparam>
        /// <returns>Коллекция спарсенных данных в виде коллекции</returns>
        public List<T> Parse<T>()
        {
            try {
                XmlNodeList nodeList = xmlSource.GetElementsByTagName(typeof(T).Name);
                return (List<T>) parseXmlNodeList(nodeList, typeof(T));
            }
            catch (XmlParserParsingAttributeException ex) {
                throw new Exception("Непредвиденная ошибка парсинга данных атрибутов", ex);
            }
            catch (XmlParserParsingDataTypeException ex) {
                throw new Exception("Непредвиденная ошибка парсинга данных полей класса", ex);
            }
            catch (Exception ex) {
                throw new Exception("Непредвиденное исключение в приложении", ex);
            }
        }

        /// <summary>
        /// Преобразование информации по XML-данным и прототипу класса в коллекцию
        /// </summary>
        /// <param name="nodeList">Список XML-записей выбранного класса</param>
        /// <param name="parseType">Тип парсимого объекта</param>
        /// <returns>Спарсенная коллекция объектов</returns>
        private object parseXmlNodeList(XmlNodeList nodeList, Type parseType)
        {
            Type listType = typeof(List<>).MakeGenericType(parseType);
            object list = listType.GetConstructor(new Type[] { }).Invoke(new object[] { });
            MethodInfo listAddMethod = listType.GetMethod("Add");

            foreach (XmlNode node in nodeList)
            {
                listAddMethod.Invoke(list, new object[] { parseXmlNode(node, parseType) });
            }

            return list;
        }

        /// <summary>
        /// Парсинг одного XML-элемента с преобразованием к некоторому виду
        /// </summary>
        /// <typeparam name="T">Класс элемента для парсинга</typeparam>
        /// <param name="node">XML-данные об узле для парсинга</param>
        /// <returns>Спарсенный объект из XML-элемента</returns>
        private object parseXmlNode(XmlNode node, Type nodeType)
        {
            PropertyInfo[] properties = nodeType.GetProperties();
            object parsedItem = Activator.CreateInstance(nodeType, false);

            // Парсинг свойств
            foreach (PropertyInfo property in properties)
            {
                object parsedProperty = parseProperty(node, property);
                if (isParseField(property))
                    property.SetValue(parsedItem, parsedProperty);
            }

            // Парсинг атрибутов класса
            foreach (KeyValuePair<PropertyInfo, object> propertyData in parseAttributeData(node, nodeType))
            {
                propertyData.Key.SetValue(parsedItem, propertyData.Value);
            }
            
            return parsedItem;
        }

        /// <summary>
        /// Проверка на необходимость парсинга данного поля
        /// </summary>
        /// <param name="property">Поле, которое проверяется на необходимость парсинга</param>
        /// <returns>Логическое значение <b>true</b> - если поле нужно парсить,
        /// <b>false</b> - в противном случае</returns>
        private bool isParseField(PropertyInfo property)
        {
            if (property.GetCustomAttribute(typeof(XmlAttributeDontParse)) != null)
                return false;

            return true;
        }

        /// <summary>
        /// Парсинг атрибутов класса
        /// </summary>
        /// <param name="node">XML-представление объекта, который парсится</param>
        /// <param name="nodeType">Тип класса, у которого извлекаются атрибуты</param>
        /// <returns>Словарь вида "Свойство класса - данные", готовый для добавления в экземпляр класса</returns>
        private Dictionary<PropertyInfo, object> parseAttributeData(XmlNode node, Type nodeType)
        {
            Dictionary<PropertyInfo, object> sourceTargetData = new Dictionary<PropertyInfo, object>();

            // Парсинг атрибутов-полей
            foreach (XmlAttribute attribute in nodeType.GetCustomAttributes(typeof(XmlAttribute)))
            {
                KeyValuePair<PropertyInfo, object> parsedAttributeValue = parseAttribute(attribute, node, nodeType);
                if (!parsedAttributeValue.Equals(default(KeyValuePair<PropertyInfo, object>)))
                    sourceTargetData.Add(parsedAttributeValue.Key, parsedAttributeValue.Value);
            }

            // Парсинг атрибутов - значений записей
            KeyValuePair<PropertyInfo, object> classData = parseClassData(node, nodeType);
            if (!classData.Equals(default(KeyValuePair<PropertyInfo, object>)))
                sourceTargetData.Add(classData.Key, classData.Value);

            return sourceTargetData;
        }

        /// <summary>
        /// Парсинг атрибута XML-объекта в указанное поле класса
        /// </summary>
        /// <param name="attribute">Данные о выбранном атрибуте</param>
        /// <param name="node">XML-представление объекта, который парсится</param>
        /// <param name="nodeType">Тип класса, у которого извлекаются атрибуты</param>
        /// <returns>Словарь вида "Свойство класса - данные", готовый для добавления в экземпляр класса</returns>
        private KeyValuePair<PropertyInfo, object> parseAttribute(XmlAttribute attribute, XmlNode node, Type nodeType)
        {
            XmlNode attributeData = node.Attributes.GetNamedItem(attribute.AttributeSourceName);
            if (attributeData == null)
                return default(KeyValuePair<PropertyInfo, object>);

            PropertyInfo attributeInfo = nodeType.GetProperty(attribute.AttributeTargetName);
            if (attributeInfo == null)
            {
                string errorString = string.Format("Ошибка преобразования данных в тип \"{0}\"", nodeType.ToString());
                throw new XmlParserParsingAttributeException(errorString);
            }

            Type attributeField = attributeInfo.PropertyType;
            object attributeValue = parseDefaultType(attributeData.Value, attributeField);
            return new KeyValuePair<PropertyInfo, object>(nodeType.GetProperty(attribute.AttributeTargetName), attributeValue);
        }

        /// <summary>
        /// Парсинг атрибута и значения текущей выбранной записи
        /// </summary>
        /// <param name="node">XML-представление объекта, который парсится</param>
        /// <param name="nodeType">Тип класса, у которого извлекаются атрибуты</param>
        /// <returns>Пара вида "Свойство класса - данные", готовый для добавления в экземпляр класса</returns>
        private KeyValuePair<PropertyInfo, object> parseClassData(XmlNode node, Type nodeType)
        {
            XmlAttributeValue attribute = (XmlAttributeValue) nodeType.GetCustomAttribute(typeof(XmlAttributeValue));
            if (attribute == null)
                return default(KeyValuePair<PropertyInfo, object>);

            PropertyInfo property = nodeType.GetProperty(attribute.AttributeTargetName);
            // Атрибут определен, но поля в классе нет
            if (property == null)
            {
                string errorString = string.Format("Ошибка парсинга данных поля {0}", nodeType.ToString());
                throw new XmlParserParsingDataTypeException(errorString);
            }

            return new KeyValuePair<PropertyInfo, object>(property, node.InnerText);
        }

        /// <summary>
        /// Парсинг одного из свойств объекта
        /// </summary>
        /// <param name="node">XML-данные для парсинга</param>
        /// <param name="property">Свойство для парсинга</param>
        /// <returns>Спарсенное поле объекта</returns>
        private object parseProperty(XmlNode node, PropertyInfo property)
        {
            XmlNodeList nodeList = node.SelectNodes(property.Name);
            return parsePropertyType(nodeList, property.PropertyType);
        }

        /// <summary>
        /// Парсинг данных по известному типу
        /// </summary>
        /// <param name="nodeList">XML-данные для парсинга</param>
        /// <param name="propertyType">Тип для парсинга</param>
        /// <returns>Спарсенное поле объекта</returns>
        private object parsePropertyType(XmlNodeList nodeList, Type propertyType)
        {
            // Парсинг коллекции объектов
            if (propertyType.IsGenericType)
                return parseGenericType(nodeList, propertyType);

            // Парсинг простых типов и классов
            if (nodeList.Count > 0)
                return parsePropertyType(nodeList[0], propertyType);

            // Неизвестный тип
            return (propertyType.IsValueType ? Activator.CreateInstance(propertyType) : null);
        }

        /// <summary>
        /// Преобрвзование простого XML-элемента в объект
        /// </summary>
        /// <param name="node">Преобразовываемый XML-объект</param>
        /// <param name="propertyType">Тип преобразовываемого объекта</param>
        /// <returns>Спарсенный объект выбранного типа</returns>
        private object parsePropertyType(XmlNode node, Type propertyType)
        {
            // Парсинг базового типа среды
            if (isBaseType(propertyType))
                 return parseDefaultType(node.InnerText, propertyType); // В классе одно поле базового типа с таким именем

            // Парсинг пользовательского класса
            return parseXmlNode(node, propertyType);
        }

        /// <summary>
        /// Проверка типа на принадлежность к базовым
        /// </summary>
        /// <param name="type">Проверяемый тип</param>
        /// <returns>Логическое значение, показывающее принадлежность типа к базовым</returns>
        private bool isBaseType(Type type)
        {
            if (type.IsPrimitive)
                return true;

            if (type == typeof(string) || type == typeof(Decimal) || type == typeof(DateTime)
                || type == typeof(TimeSpan) || type == typeof(DateTimeOffset))
                return true;

            return false;
        }

        /// <summary>
        /// Парсинг базовых типов данных
        /// </summary>
        /// <param name="argument">Строчное представление значения</param>
        /// <param name="propertyInfo">Тип поля модели</param>
        /// <returns>Объект, полученный после преобразования</returns>
        private object parseDefaultType(string argument, Type propertyInfo)
        {
            return Convert.ChangeType(argument, propertyInfo, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Парсинг обобщенных типов данных
        /// </summary>
        /// <param name="argument">Набор данных, представляющих значение</param>
        /// <param name="propertyInfo">Тип поля модели</param>
        /// <returns>Объект, полученный после преобразования</returns>
        private object parseGenericType(XmlNodeList argument, Type propertyInfo)
        {
            Type listDataType = propertyInfo.GenericTypeArguments[0];
            Type listGenericType = typeof(List<>).MakeGenericType(listDataType);

            object parsedObjects = listGenericType.GetConstructor(new Type[] { }).Invoke(new object[] { });
            MethodInfo mi = listGenericType.GetMethod("Add");

            foreach (XmlNode node in argument)
            {
                mi.Invoke(parsedObjects, new object[] { parsePropertyType(node, listDataType) });
            }

            return parsedObjects;
        }
    }
}