﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Data.Entity;
using System.Data.SqlServerCe;
using System.Data.Entity.Validation;

using XmlParser.Models;
using System.Data.Entity.Core;

namespace XmlParser
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlSource xmlSource = new XmlSource("E:\\div_gifts.xml");
            XmlParser parser = new XmlParser(xmlSource);
            List<Models.offer> offers = parser.Parse<Models.offer>();
            List<Models.category> categories = parser.Parse<Models.category>();

            List<Vendor> vendors = new List<Vendor>();
            List<Currency> currencies = new List<Currency>();

            foreach (category oneCategoryItem in categories)
            {
                oneCategoryItem.parent = categories.FirstOrDefault(x => x.categoryId == oneCategoryItem.parentId);
            }

            foreach (offer oneOfferItem in offers)
            {
                List<category> offerCategories = new List<category>();
                foreach (int categoryId in oneOfferItem.categoryId)
                {
                    offerCategories.Add(categories.FirstOrDefault(x => x.categoryId == categoryId));
                }
                oneOfferItem.categories = offerCategories;

                // Создание справочника Vendor - каждый Vendor уникален
                Vendor vendor = new Vendor() { vendorId = oneOfferItem.id, vendor = oneOfferItem.vendor, vendorCode = oneOfferItem.vendorCode };
                vendor.Offer = oneOfferItem;
                vendors.Add(vendor);

                // Создание справочника валют
                Currency currency = new Currency() { currencyId = oneOfferItem.id, name = oneOfferItem.currencyId };
                currency.Offer = oneOfferItem;
                currencies.Add(currency);
            }

            using (var database = new OzonShopContext())
            {
                database.Database.Delete();

                database.Categories.AddRange(categories);
                database.Currencies.AddRange(currencies);
                database.Vendors.AddRange(vendors);
                try
                {
                    database.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {
                        Console.WriteLine(eve.Entry.Entity);
                        foreach (var eveIn in eve.ValidationErrors)
                        {
                            Console.WriteLine("Property \"{0}\" has error: {1}", eveIn.PropertyName, eveIn.ErrorMessage);
                        }
                    }
                }

                Console.WriteLine("Данные категорий успешно сохранены!");

                database.Offers.AddRange(offers);
                Console.WriteLine("Данные товаров успешно сохранены!");
                try
                {
                    database.SaveChanges();
                }
                catch (System.Exception ex)
                {
                    Exception excp = ex.InnerException.InnerException;
                    foreach (var data in excp.Data)
                    {
                        Console.WriteLine(data);
                    }
                }              
            }

            Console.WriteLine("Парсинг успешно завершен!");
            Console.ReadKey();            
        }
    }
}
