﻿using System;
using System.IO;
using System.Xml;

namespace XmlParser
{
    /// <summary>
    /// Демонстрационный класс-поставщик информации для БД из XML-файла
    /// </summary>
    class XmlSource : IXmlDataProvider
    {
        private string path;

        /// <summary>
        /// Инициализация класса строкой, указывающей на файл для парсинга
        /// </summary>
        /// <param name="path">Путь к файлу для парсинга</param>
        public XmlSource(string path)
        {
            this.path = path;
        }

        public XmlNodeList GetElementsByTagName(string tagName)
        {
            XmlDocument doc = new XmlDocument();

            if (File.Exists(path))
                doc.Load(path);

            return doc.GetElementsByTagName(tagName);
        }
    }
}
