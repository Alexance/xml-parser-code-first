﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace XmlParser.Models.offerElements
{
    [XmlAttributeValue("Time")]
    public class OrderingTime
    {
        [Key]
        public int id { get; set; }
        public string Time { get; set; }
    }
}
