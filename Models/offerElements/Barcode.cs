﻿using System;
using System.ComponentModel.DataAnnotations;

namespace XmlParser.Models.OfferElements
{
    public class Barcode
    {
        public int barcodeId { get; set; }
        [MaxLength(50)]
        public string value { get; set; }
    }
}
