﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using XmlParser.Models;

namespace XmlParser.Models.offerElements
{
    [XmlAttributeValue("ParamValue")]
    [XmlAttribute("name", "Name")]
    public class Param
    {
        [Key]
        public int id { get; set; }
        public string ParamValue { get; set; }
        public string Name { get; set; }
    }
}
