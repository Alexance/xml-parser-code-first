﻿using System;
using System.ComponentModel.DataAnnotations;

namespace XmlParser.Models.OfferElements
{
    public class OrdernigTime
    {
        [Key]
        public int orderingId { get; set; }
        public string value { get; set; }
    }
}
