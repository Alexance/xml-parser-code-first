﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using System.Data.Entity.Validation;

using XmlParser.Models;

namespace XmlParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start time: {0}", DateTime.Now.ToString());

            XmlParser parser = new XmlParser(@"E:\div_gifts.xml", Encoding.GetEncoding(1251));
            yml_catalog data = parser.Parse<yml_catalog>();

            XmlDataConverter xmlConverter = new XmlDataConverter(data);
            List<Category> categories = xmlConverter.ParseCategories();

            Console.WriteLine("Categories parsing ended at: {0}", DateTime.Now.ToString());
            Console.WriteLine("Total categories parsed: {0}", Convert.ToString(categories.Count));
            Console.WriteLine();
            Console.WriteLine("Offers parsing started at: {0}", DateTime.Now.ToString());

            List<Offer> offers = xmlConverter.ParseOffers(categories);

            Console.WriteLine("Offers parsing ended at: {0}", DateTime.Now.ToString());
            Console.WriteLine("Total offers parsed: {0}", Convert.ToString(offers.Count));

            Console.WriteLine();
            Console.WriteLine("Adding to database started at: {0}", DateTime.Now.ToString());

            using (OzonDatabaseContext context = new OzonDatabaseContext())
            {
                context.Database.Delete();

                context.Categories.AddRange(categories);
                context.Offers.AddRange(offers);
                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                	foreach (var one in ex.EntityValidationErrors)
                	{
                        foreach (var oneItem in one.ValidationErrors)
                            Console.WriteLine("Property: {0}, Message: {1}", oneItem.PropertyName, oneItem.ErrorMessage);
                	}
                }
            }

            Console.WriteLine("Adding to database ended at: {0}", DateTime.Now.ToString());
            Console.ReadKey(true);
        }
    }
}
