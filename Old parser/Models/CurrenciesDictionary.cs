﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XmlParser.Models
{
    public class Currency
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.None)]
        public int currencyId { get; set; }

        [MaxLength(10)]
        public string name { get; set; }
        
        [Required]
        [ForeignKey("currencyId")]
        public virtual offer Offer { get; set; }
    }
}
