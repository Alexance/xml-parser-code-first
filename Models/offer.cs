﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using XmlParser.Models.OfferElements;

namespace XmlParser.Models
{
    public class Offer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }
        public bool available { get; set; }
        [MaxLength(300)]
        public string url { get; set; }
        public bool delivery { get; set; }
        [MaxLength(250)]
        public string name { get; set; }
        public string description { get; set; }
        [MaxLength(50)]
        public string model { get; set; }
        [MaxLength(50)]
        public string type { get; set; }

        public Currency Currency { get; set; }
        public Vendor Vendor { get; set; }
        public PublisherInfo PublisherInfo { get; set; }

        public ICollection<Category> Categories { get; set; }
        public ICollection<Picture> Pictures { get; set; }
        public ICollection<Param> Params { get; set; }
        public ICollection<OrdernigTime> OrderingTimes { get; set; }
        public ICollection<Barcode> Barcodes { get; set; }
    }
}
