﻿using System;
using System.Runtime.Serialization;

namespace XmlParser
{
    /// <summary>
    /// Исключение, возникающие при появлении ошибки в
    /// ходе преобразования из текстового в указанный тип
    /// </summary>
    public class XmlParserParsingDataTypeException : Exception
    {
        // Переопределенные конструкторы для возможности использования
        // исключения в любой ситуации
        public XmlParserParsingDataTypeException() : base() { }
        public XmlParserParsingDataTypeException(string message) : base(message) { }
        public XmlParserParsingDataTypeException(string message, Exception innerException) : base(message, innerException) { }
        public XmlParserParsingDataTypeException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// Исключение, возникающее при появлении ошибки в
    /// ходе парсинга установленных пользователем атрибутов класса
    /// </summary>
    public class XmlParserParsingAttributeException : Exception
    {
        public XmlParserParsingAttributeException() : base() { }
        public XmlParserParsingAttributeException(string message) : base(message) { }
        public XmlParserParsingAttributeException(string message, Exception innerException) : base(message, innerException) { }
        public XmlParserParsingAttributeException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
