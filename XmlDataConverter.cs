﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using XmlParser.Models;
using XmlParser.Models.OfferElements;

namespace XmlParser
{
    /// <summary>
    /// Конвертирует информацию из стандартного представления в необходимое
    /// представление согласно модели
    /// </summary>
    public class XmlDataConverter
    {
        private yml_catalog data;

        /// <summary>
        /// Инициализирует класс значением с данными, которые подлежат разбору
        /// </summary>
        /// <param name="data"></param>
        public XmlDataConverter(yml_catalog data)
        {
            this.data = data;
        }

        /// <summary>
        /// Парсит данные категорий и составляет связи между ними
        /// </summary>
        /// <returns>Коллекция спарсенных объектов типа <b>Category</b></returns>
        public List<Category> ParseCategories()
        {
            List<Category> categories = new List<Category>();

            foreach (var oneShop in data.shop)
            {
                foreach (var oneYmlCategory in oneShop.categories)
                {
                    Category category = new Category() { categoryId = Convert.ToInt32(oneYmlCategory.id), value = oneYmlCategory.Value };
                    category.ParentCategory = categories.FirstOrDefault(x => x.categoryId == Convert.ToInt32(oneYmlCategory.parentId));
 
                    categories.Add(category);
                }
            }

            return categories;
        }

        /// <summary>
        /// Парсит данные о товарах
        /// </summary>
        /// <param name="categories">Категории, с которыми будут связываться товары</param>
        /// <returns>Спарсенные товары документа</returns>
        public List<Offer> ParseOffers(List<Category> categories)
        {
            List<Offer> offers = new List<Offer>();
            var parsedYmlOffers = this.ParseYmlOffers();

            foreach (var ymlOffer in parsedYmlOffers)
            {
                Offer offer = this.parseOffer(ymlOffer);
                offer.Categories = this.parseOfferCategories(ymlOffer, categories);
                offers.Add(offer);
            }

            return offers;
        }

        /// <summary>
        /// Парсит данные из общего класса в отдельные классы
        /// </summary>
        /// <returns>Спарсенная коллекция с объектами стандартного типа <b>yml_catalogShopOffersOffer</b></returns>
        private List<yml_catalogShopOffersOffer> ParseYmlOffers()
        {
            List<yml_catalogShopOffersOffer> ymlOffers = new List<yml_catalogShopOffersOffer>();

            foreach (var oneShop in data.shop)
            {
                ymlOffers.AddRange(oneShop.offers);
            }

            return ymlOffers;
        }

        /// <summary>
        /// Парсит данные о категориях для текущего товара
        /// </summary>
        /// <param name="ymlOffer">Данные о товаре для парсинга</param>
        /// <param name="categories">Категории, к которым может принадлежать товар</param>
        /// <returns>Коллекция спарсенных категорий товара</returns>
        private List<Category> parseOfferCategories(yml_catalogShopOffersOffer ymlOffer, List<Category> categories)
        {
            List<Category> offerCategories = new List<Category>();

            foreach (var category in ymlOffer.categoryId)
            {
                offerCategories.Add(categories.First(x => x.categoryId == Convert.ToInt32(category.Value)));
            }

            return offerCategories;
        }

        /// <summary>
        /// Парсит <b>yml_shopOffersOffer</b> в класс сущности <b>Offer</b>
        /// </summary>
        /// <param name="ymlOffer">Данные в стандартной форме каталога для парсинга</param>
        /// <returns>Спарсенный объект в виде экземпляра класса <b>Offer</b></returns>
        private Offer parseOffer(yml_catalogShopOffersOffer ymlOffer)
        {
            Offer offer = new Offer();

            offer.id = Convert.ToInt32(ymlOffer.id);
            offer.available = Convert.ToBoolean(ymlOffer.available);
            offer.delivery = Convert.ToBoolean(ymlOffer.delivery);
            offer.url = ymlOffer.url;
            offer.name = ymlOffer.name;
            offer.description = ymlOffer.description;
            offer.model = ymlOffer.model;
            offer.type = ymlOffer.type;

            offer.Pictures = this.parsePictures(ymlOffer);
            offer.Params = this.parseParams(ymlOffer);
            offer.OrderingTimes = this.parseOrderingTime(ymlOffer);
            offer.Barcodes = this.parsingBarcodes(ymlOffer);

            offer.PublisherInfo = this.parsePublisherInfo(ymlOffer);
            offer.Vendor = this.parseVendor(ymlOffer);
            offer.Currency = this.parseCurrency(ymlOffer);

            return offer;
        }

        /// <summary>
        /// Парсит данные об изображениях текущего товара
        /// </summary>
        /// <param name="ymlOffer">Данные о товаре для парсинга</param>
        /// <returns>Спарсенная коллекция данных типа <b>Picture</b></returns>
        private List<Picture> parsePictures(yml_catalogShopOffersOffer ymlOffer)
        {
            List<Picture> pictures = new List<Picture>();
            yml_catalogShopOffersOfferPicture[] ymlPictures = ymlOffer.picture;

            if (ymlPictures == null)
                return null;

            foreach (var oneYmlPicture in ymlPictures)
            {
                pictures.Add(new Picture { url = oneYmlPicture.Value });
            }

            return pictures;
        }

        /// <summary>
        /// Парсит данные о параметрах выбранного товара
        /// </summary>
        /// <param name="ymlOffer">Данные о товаре для парсинга</param>
        /// <returns>Спарсенная коллекция данных типа <b>Param</b></returns>
        private List<Param> parseParams(yml_catalogShopOffersOffer ymlOffer)
        {
            List<Param> parameters = new List<Param>();
            yml_catalogShopOffersOfferParam[] ymlParams = ymlOffer.param;

            if (ymlParams == null)
                return null;

            foreach (var oneYmlParam in ymlParams)
            {
                parameters.Add(new Param { name = oneYmlParam.name, value = oneYmlParam.Value, unit = oneYmlParam.unit });
            }

            return parameters;
        }

        /// <summary>
        /// Парсинг даных данных о времени отгрузки для каждого товара
        /// </summary>
        /// <param name="ymlOffer">Данные о товаре для парсинга</param>
        /// <returns>Спарсенная коллекция данных типа <b>OrderingTime</b></returns>
        private List<OrdernigTime> parseOrderingTime(yml_catalogShopOffersOffer ymlOffer)
        {
            List<OrdernigTime> orderingTimes = new List<OrdernigTime>();
            yml_catalogShopOffersOfferOrderingTime[] ymlTimes = ymlOffer.orderingTime;

            foreach (var oneYmlTime in ymlTimes)
            {
                orderingTimes.Add(new OrdernigTime { value = oneYmlTime.ordering });
            }

            return orderingTimes;
        }

        /// <summary>
        /// Парсинг данных о коде товара
        /// </summary>
        /// <param name="ymlOffer">Данные о товаре для парсинга</param>
        /// <returns>Спарсенные данные о кодах товара</returns>
        private List<Barcode> parsingBarcodes(yml_catalogShopOffersOffer ymlOffer)
        {
            List<Barcode> barcodes = new List<Barcode>();
            yml_catalogShopOffersOfferBarcode[] ymlBarcodes = ymlOffer.barcode;

            if (ymlBarcodes == null)
                return null;

            foreach (var oneYmlBarcode in ymlBarcodes)
            {
                barcodes.Add(new Barcode { value = oneYmlBarcode.Value });
            }

            return barcodes;
        }

        /// <summary>
        /// Парсинг данных о поставщике товара
        /// </summary>
        /// <param name="ymlOffer">Данные о товаре для парсинга</param>
        /// <returns>Спарсенные данные о поставщике товара</returns>
        private Vendor parseVendor(yml_catalogShopOffersOffer ymlOffer)
        {
            return new Vendor() { vendorId = Convert.ToInt32(ymlOffer.id), vendor = ymlOffer.vendor, vendorCode = ymlOffer.vendorCode };
        }

        /// <summary>
        /// Парсинг данных о стоимости товара
        /// </summary>
        /// <param name="ymlOffer">Даннеы о товаре</param>
        /// <returns>Спарсенные данные о стоимости товара</returns>
        private Currency parseCurrency(yml_catalogShopOffersOffer ymlOffer)
        {
            return new Currency() { currencyId = Convert.ToInt32(ymlOffer.id), value = ymlOffer.currencyId, price = Convert.ToDouble(ymlOffer.price, CultureInfo.InvariantCulture) };
        }

        /// <summary>
        /// Парсинг данных об издателе товара
        /// </summary>
        /// <param name="ymlOffer">Данные о товаре для парсинга</param>
        /// <returns>Спарсенные данные об издателе товара</returns>
        private PublisherInfo parsePublisherInfo(yml_catalogShopOffersOffer ymlOffer)
        {
            PublisherInfo pi = new PublisherInfo();

            pi.publisherId = Convert.ToInt32(ymlOffer.id);
            pi.author = ymlOffer.author;
            pi.ISBN = ymlOffer.ISBN;
            pi.page_extent = Convert.ToInt32(ymlOffer.page_extent);
            pi.publisher = ymlOffer.publisher;
            pi.series = ymlOffer.series;
            pi.year = Convert.ToInt32(ymlOffer.year);

            return pi;
        }
    }
}