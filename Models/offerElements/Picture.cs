﻿using System;

namespace XmlParser.Models.OfferElements
{
    public class Picture
    {
        public int pictureId { get; set; }
        public string url { get; set; }
    }
}
