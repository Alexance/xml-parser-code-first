﻿using System;
using System.Collections.Generic;

using XmlParser.Models;
using System.Data.Entity;

namespace XmlParser
{
    /// <summary>
    /// Класс-контекст, отвечающий за взаимодействие данных и БД
    /// </summary>
    public class OzonShopContext : DbContext
    {
        public DbSet<offer> Offers { get; set; }
        public DbSet<category> Categories { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Currency> Currencies { get; set; }

    }
}
