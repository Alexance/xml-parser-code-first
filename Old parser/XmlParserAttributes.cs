﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlParser
{
    /// <summary>
    /// Атрибут для указания атрибута-источника в XML-представлении и имени поля в конечном экземпляре класса
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class XmlAttribute : Attribute
    {
        // Имя атрибута-источника в классе или поле
        public string AttributeSourceName { get; private set; }
        // Имя поля в классе, куда будет сохранен атрибут
        public string AttributeTargetName { get; private set; }

        /// <summary>
        /// Инициализация атрибута, указывающего имя поля класса,
        /// куда будет сохранены данные в объекте, полученные из XML
        /// </summary>
        /// <param name="AttributeSourceName">Имя атрибута-источника в классе или поле</param>
        /// <param name="XmlAttributeTarget">Имя поля в классе, куда будет сохранен атрибут</param>
        public XmlAttribute(string AttributeSourceName, string AttributeTargetName)
        {
            this.AttributeSourceName = AttributeSourceName;
            this.AttributeTargetName = AttributeTargetName;
        }
    }

    /// <summary>
    /// Атрибут, показывающий место, куда необходимо парсить данные из тела записи XML
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class XmlAttributeValue : Attribute
    {
        // Имя поля класса, куда будет сохранено значение записи
        public string AttributeTargetName { get; private set; }

        /// <summary>
        /// Инициализация атрибута, указывающего имя поля класса,
        /// куда будет сохранены внутренние данные XML-записи
        /// </summary>
        /// <param name="AttributeTargetName"></param>
        public XmlAttributeValue(string AttributeTargetName)
        {
            this.AttributeTargetName = AttributeTargetName;
        }
    }

    /// <summary>
    /// Атрибут, указывающий XML-парсеру о пропуске данного поля при парсинге
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class XmlAttributeDontParse : Attribute
    {
        /// <summary>
        /// Пометка поля об отсутствии необходимости парсинга
        /// </summary>
        public XmlAttributeDontParse() { }
    }
}
