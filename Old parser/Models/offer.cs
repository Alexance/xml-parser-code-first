﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using XmlParser.Models.offerElements;

namespace XmlParser.Models
{
    [XmlAttribute("id", "id")]
    [XmlAttribute("available", "available")]
    public class offer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }
        public bool available { get; set; }
        [MaxLength(500)]
        public string url { get; set; }
        public double price { get; set; }
        public virtual List<Picture> picture { get; set; }
        public bool delivery { get; set; }

        [NotMapped]
        public string currencyId { get; set; }
        [XmlAttributeDontParse]
        public virtual Currency currency { get; set; }

        [NotMapped]
        public List<int> categoryId { get; set; }
        [XmlAttributeDontParse]
        public virtual List<category> categories { get; set; }

        [NotMapped]
        public string vendor { get; set; }
        [NotMapped]
        public string vendorCode { get; set; }
        [XmlAttributeDontParse]
        public virtual Vendor vendorName { get; set; }

        [MaxLength(250)]
        public string name { get; set; }
        public string description { get; set; }
        [MaxLength(100)]
        public string barcode { get; set; }

        // Параметры с атрибутами
        public virtual List<OrderingTime> orderingTime { get; set; }
        public virtual List<Param> param { get; set; }
    }
}
