﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace XmlParser.Models
{
    [XmlAttributeValue("PictureUrl")]
    public class Picture
    {
        [Key]
        public int id { get; set; }
        public string PictureUrl { get; set; }
    }
}
