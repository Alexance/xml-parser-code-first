﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XmlParser.Models
{
    public class Vendor
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.None)]
        public int vendorId { get; set; }

        [MaxLength(100)]
        public string vendor { get; set; }
        [MaxLength(100)]
        public string vendorCode { get; set; }
        
        [Required]
        [ForeignKey("vendorId")]
        public virtual offer Offer { get; set; }
    }
}
