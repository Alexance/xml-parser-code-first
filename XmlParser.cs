﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;

namespace XmlParser
{
    /// <summary>
    /// Парсер данных из XML-файла
    /// </summary>
    public class XmlParser
    {
        private readonly StreamReader stream;

        /// <summary>
        /// Инициализация источника данных
        /// </summary>
        /// <param name="stream">Тип для парсинга</param>
        public XmlParser(StreamReader stream)
        {
            this.stream = stream;
        }

        /// <summary>
        /// Инициализация источника данных
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <param name="encoding">Кодировка источника</param>
        public XmlParser(string path, Encoding encoding)
        {
            this.stream = new StreamReader(path, encoding);
        }

        /// <summary>
        /// Парсинг данных по коду класса
        /// </summary>
        /// <typeparam name="T">Класс, представляющий данные в XML-файле</typeparam>
        /// <returns>Спарсенные данные из файла</returns>
        public T Parse<T>() where T : class 
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            T data = (T) serializer.Deserialize(stream);

            return data;
        }
    }
}
