﻿using System;
using System.Xml;

namespace XmlParser
{
    /// <summary>
    /// Интерфейс обработки данных XML-документа
    /// </summary>
    public interface IXmlDataProvider
    {
        /// <summary>
        /// Возвращает список всех элементов с заданным именем
        /// </summary>
        /// <param name="tagName">Имя тега</param>
        /// <returns>Список всех тегов с заданным именем</returns>
        XmlNodeList GetElementsByTagName(string tagName);
    }
}
