﻿using System;
using System.Collections.Generic;

using System.Data.Entity;
using XmlParser.Models;

namespace XmlParser
{
    public class OzonDatabaseContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Offer> Offers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().ToTable("Categories");

            base.OnModelCreating(modelBuilder);
        }
    }
}
